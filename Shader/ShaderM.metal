//
//  ShaderM.metal
//  Shader
//
//  Created by 郑红 on 2021/4/21.
//  Copyright © 2021 com.zhenghong. All rights reserved.
//

#include <metal_stdlib>
#import "ShaderCommon.h"
using namespace metal;

typedef struct
{
    float4 clipSpacePosition [[position]];
    float2 textureCoordinate;
} RasterizerData;

bool validateArea(ShaderRegionArea area, uint2 gid) {
    if (gid.x < area.minX) {
        return false;
    }
    if (gid.y < area.minY) {
        return false;
    }
    if (gid.x > area.maxX) {
        return false;
    }
    if (gid.y > area.maxY) {
        return false;
    }
    return true;
}

bool vec4Equal(vector_uint4 left, vector_uint4 right) {
    return left[0] == right[0] && left[1] == right[1]  && left[2] == right[2] && left[3] == right[3];
}



vertex RasterizerData
vertexFunc(uint vertexId [[vertex_id]],
           constant ShaderVertex *vertexArray [[ buffer(ShaderVertexInputIndexVertex) ]],
           constant float4x4 *matrix [[ buffer(ShaderVertexInputIndexMatrix) ]]
           ) {
    
    
    RasterizerData out;
    float4x4 matrixxx = float4x4(*matrix);
    out.clipSpacePosition = matrixxx * vertexArray[vertexId].position;
    out.textureCoordinate = vertexArray[vertexId].color;
    
    return out;
}


fragment float4
fragmentFunc (RasterizerData input [[stage_in]],
              texture2d<half> colorTexture [[ texture(ShaderFragmentInputIndexVertex) ]]) {
    constexpr sampler textureSampler (mag_filter::linear,
                                      min_filter::linear); // sampler是采样器
    
    half4 colorSample = colorTexture.sample(textureSampler, input.textureCoordinate);
    return float4(colorSample);
}

constant float3 kRec709Luma = float3(0.2126, 0.7152, 0.0722);





kernel void make_color_painted(
                               texture2d<float,access::write>  paintingTexture  [[ texture(ShaderComputeTextureIndexPainting) ]],
                               texture2d<float,access::read>  paintedTexture  [[ texture(ShaderComputeTextureIndexPainted) ]],
                               constant ShaderRegion *region [[ buffer(ShaderComputeBufferIndexRegion) ]],
                               constant uint32_t *regionTexture     [[ buffer(ShaderComputeTextureIndexRegion) ]],
                               uint2 gid                                    [[ thread_position_in_grid ]])
{
    ShaderRegion colorRegion = ShaderRegion(*region);
    ShaderRegionArea area = colorRegion.area;
    if(!validateArea(area, gid)) {
        return;
    }
    int32_t location = gid.y * colorRegion.pixWidth + gid.x;
    uint16_t region111 = (uint16_t)(regionTexture[location] & 0XFFFF);
    if (region111 != colorRegion.regionColor) {
        //        paintingTexture.write(float4(1.0,0.34,0.14,1.0), gid);
        return;
    }
    
    float4 painted = paintedTexture.read(gid);
    //    float  r = dot(painted.rgb, kRec709Luma);
    //    paintingTexture.write(float4(r,r,r,1.0), gid);
    paintingTexture.write(painted, gid);
}

kernel void make_color_main(
                            texture2d<float,access::write>  paintingTexture  [[ texture(ShaderComputeTextureIndexPainting) ]],
                            constant ShaderRegion *region [[ buffer(ShaderComputeBufferIndexRegion) ]],
                            constant uint32_t *regionTexture     [[ buffer(ShaderComputeTextureIndexRegion) ]],
                            uint2 gid                                                               [[ thread_position_in_grid ]])
{
    ShaderRegion colorRegion = ShaderRegion(*region);
    ShaderRegionArea area = colorRegion.area;
    if(!validateArea(area, gid)) {
        return;
    }
    int32_t location = gid.y * colorRegion.pixWidth + gid.x;
    uint16_t region111 = (uint16_t)(regionTexture[location] & 0XFFFF);
    if (region111 != colorRegion.regionColor) {
        //        paintingTexture.write(colorRegion.color, gid);
        return;
    }
    paintingTexture.write(colorRegion.color, gid);
}

kernel void make_color_gray(texture2d<float,access::write>  paintingTexture  [[ texture(ShaderComputeTextureIndexPainting) ]],
                            texture2d<float,access::read>  paintedTexture  [[ texture(ShaderComputeTextureIndexPainted) ]],
                            uint2 gid  [[ thread_position_in_grid ]]) {
    float4 inColor = paintedTexture.read(gid);
    float gray = dot(inColor.rgb, kRec709Luma);
    paintingTexture.write(float4(gray, gray, gray, 1.0), gid);
}
