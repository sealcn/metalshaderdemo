//
//  BaseViewController.h
//  Shader
//
//  Created by mac on 2021/4/24.
//  Copyright © 2021 com.zhenghong. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Metal/Metal.h>
#import <MetalKit/MetalKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface BaseViewController : UIViewController<MTKViewDelegate>

@property (nonatomic, strong) id<MTLDevice> device;
@property (nonatomic, strong) id<MTLLibrary> library;
@property (nonatomic, strong) id<MTLCommandQueue> commandQueue;

@property (nonatomic, strong) MTKView *mtview;

@property (nonatomic, assign) vector_uint2 viewPort;


- (void)drawWithBuffer:(id<MTLCommandBuffer>)buffer view:(MTKView *)view;


- (id<MTLTexture>)loadImageTexture:(NSString *)imageName type:(NSString *)type;


@end

NS_ASSUME_NONNULL_END
