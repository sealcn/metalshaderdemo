//
//  ColorBoxModel.h
//  Shader
//
//  Created by mac on 2021/4/24.
//  Copyright © 2021 com.zhenghong. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface ColorBoxModel : NSObject
@property (nonatomic, assign) float   minX;
@property (nonatomic, assign) float   minY;
@property (nonatomic, assign) float   maxX;
@property (nonatomic, assign) float   maxY;

@property (nonatomic, copy) NSString *number;

@end

NS_ASSUME_NONNULL_END
