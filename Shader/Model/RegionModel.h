//
//  RegionModel.h
//  Shader
//
//  Created by mac on 2021/4/24.
//  Copyright © 2021 com.zhenghong. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface RegionModel : NSObject

@property (nonatomic, assign) u_int16_t colorNumber;
@property (nonatomic, assign) UInt32   x;
@property (nonatomic, assign) UInt32   y;
@property (nonatomic, assign) UInt32   size;
@property (nonatomic, copy) NSString *color;
@property (nonatomic, assign) BOOL iscolored;

@end

NS_ASSUME_NONNULL_END
