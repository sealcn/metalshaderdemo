//
//  SceneDelegate.h
//  Shader
//
//  Created by 郑红 on 2021/4/21.
//  Copyright © 2021 com.zhenghong. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SceneDelegate : UIResponder <UIWindowSceneDelegate>

@property (strong, nonatomic) UIWindow * window;

@end

