//
//  ShaderCommon.h
//  Shader
//
//  Created by 郑红 on 2021/4/21.
//  Copyright © 2021 com.zhenghong. All rights reserved.
//

#ifndef ShaderCommon_h
#define ShaderCommon_h

#include <simd/simd.h>

typedef struct {
    vector_float4 position;
    vector_float2 color;
} ShaderVertex;

typedef enum {
    ShaderVertexInputIndexVertex = 0,
    ShaderVertexInputIndexMatrix = 1
} ShaderVertexInputIndex;

typedef enum {
    ShaderFragmentInputIndexVertex = 0
} ShaderFragmentInputIndex;


typedef enum {
    ShaderComputeTextureIndexPainting = 0,
    ShaderComputeTextureIndexPainted = 1
} ShaderComputeTextureIndex;

typedef enum {
    ShaderComputeTextureIndexRegion = 0,
    ShaderComputeBufferIndexRegion = 1,
} ShaderComputeBufferIndex;

typedef struct {
    float minX;
    float minY;
    float maxX;
    float maxY;
} ShaderRegionArea;

typedef struct {
    ShaderRegionArea area;
    uint16_t regionColor;
    vector_float4 color;
    int pixWidth;
} ShaderRegion;


#endif /* ShaderCommon_h */
