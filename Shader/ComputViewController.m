//
//  ComputViewController.m
//  Shader
//
//  Created by mac on 2021/4/24.
//  Copyright © 2021 com.zhenghong. All rights reserved.
//

#import "ComputViewController.h"
#import <Metal/Metal.h>
#import <MetalKit/MetalKit.h>
#import "ShaderCommon.h"
#import "UIImage+Bundle.h"
#import "NSString+Hex.h"

#import "ColorBoxLoader.h"
#import "RegionLoader.h"

#import <simd/simd.h>

typedef NS_OPTIONS(NSUInteger, ShaderType) {
    ShaderTypeNormal = 1 << 0,
    ShaderTypeColored = 1 << 1,
    ShaderTypeGray = 1 << 2,
};

#define PIDNormal @"5bbf259da28896000192b828"
#define PIDColored @"600008bb958ce900017563ee"
#define PIDGrayScale @"606189fb01d79c000109d61d"

typedef struct {
    UInt8 b;
    UInt8 a;
} Color;

@interface ComputViewController ()<MTKViewDelegate>
{
    ShaderVertex _vertexs[6];
}

@property (nonatomic, strong) id<MTLRenderPipelineState> renderPipelineState;
@property (nonatomic, strong) id<MTLRenderPipelineState> originPipelineState;

@property (nonatomic, strong) id<MTLComputePipelineState> computePipelineState;
@property (nonatomic, strong) id<MTLComputePipelineState> grayPipelineState;



@property (nonatomic, strong) id<MTLTexture> paintingTexture;
@property (nonatomic, strong) id<MTLTexture> paintedTexture;

@property (nonatomic, strong) id<MTLTexture> originTexture;

@property (nonatomic, strong) id<MTLBuffer> regionTexture;
@property (nonatomic, strong) id<MTLBuffer> regionBuffer;


@property (nonatomic, assign) simd_float4x4 matrix;


@property (nonatomic, copy) NSDictionary<NSString *, RegionModel *> * regionDic;
@property (nonatomic, copy) NSDictionary<NSString *, ColorBoxModel *> * colorDic;

@property (nonatomic, assign) CGFloat originScale;

@property (nonatomic, assign) ShaderType shaderType;

@property (nonatomic, copy) NSString *pid;
@property (nonatomic, assign) uint pixWidth;

@property (nonatomic, strong) UIButton *resetMatrixBtn;

@end

@implementation ComputViewController


- (NSString *)pid {
    if (self.shaderType & ShaderTypeColored) {
        return PIDColored;
    }
    if (self.shaderType & ShaderTypeGray) {
        return PIDGrayScale;
    }
    if (self.shaderType & ShaderTypeNormal) {
        return PIDNormal;
    }
    return @"";
}

- (uint)pixWidth {
    if (self.shaderType & ShaderTypeNormal) {
        return 1024;
    }
    return 2048;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    _shaderType = ShaderTypeGray;
    [self initPipelineState];
    [self initTetxure];
    [self initTranformMatrix];
    [self initShaderVertexs];
    [self addGesture];
    [self initData];
    [self initRegionTexture];
    [self addResetMatrixButton];
}


#pragma mark - reset matrix
- (void)addResetMatrixButton {
    _resetMatrixBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    _resetMatrixBtn.frame = CGRectMake(30, 30, 50, 50);
    [_resetMatrixBtn setTitle:@"Reset" forState:UIControlStateNormal];
    [_resetMatrixBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [_resetMatrixBtn addTarget:self action:@selector(handleResetButton) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:_resetMatrixBtn];
}

- (void)handleResetButton {
    [self initTranformMatrix];
}

#pragma mark - data
- (void)initData {
    _regionDic = [RegionLoader load:self.pid];
    _colorDic  = [ColorBoxLoader load:self.pid];
}

- (void)initRegionTexture {
    UIImage *regionImage = [UIImage bundle:[self.pid stringByAppendingString:@"_region"] type:@"png"];
    
    CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceRGB();
    if (!colorSpace) {
    }
    u_int32_t totalPixels = self.pixWidth * self.pixWidth;
    u_int32_t *regionImageDataBuffer = (u_int32_t *)malloc(sizeof(u_int32_t) * totalPixels);
    CGImageAlphaInfo imageAlphaInfo = (CGImageAlphaInfo)(kCGImageAlphaPremultipliedFirst | kCGBitmapByteOrder32Host);
    CGContextRef imageCtx = CGBitmapContextCreateWithData(regionImageDataBuffer, self.pixWidth, self.pixWidth, 8, 4 * self.pixWidth, colorSpace, imageAlphaInfo, nil, NULL);
    if (!imageCtx) {
        CGColorSpaceRelease(colorSpace);
        NSAssert(NO, @"init region data from region image error.");
    }
    
    CGContextDrawImage(imageCtx, CGRectMake(0.0, 0.0, self.pixWidth, self.pixWidth), regionImage.CGImage);
    if (regionImageDataBuffer == NULL) {
        CGColorSpaceRelease(colorSpace);
        CGContextRelease(imageCtx);

        NSAssert(NO, @"init region data from region image error.");
    }
    
//    _regionBufferData = [NSData dataWithBytesNoCopy:regionImageDataBuffer length:sizeof(u_int32_t) * totalPixels];
    
//    u_int32_t  *_regionBuffer = (u_int32_t *)_regionBufferData.bytes;
    self.regionTexture = [self.device newBufferWithBytes:regionImageDataBuffer length:sizeof(u_int32_t) * totalPixels options:MTLResourceStorageModeShared];
    CGColorSpaceRelease(colorSpace);
    CGContextRelease(imageCtx);
    
}

#pragma mark - Gesture
- (void)addGesture {
    UIPinchGestureRecognizer *pinchGes = [[UIPinchGestureRecognizer alloc] initWithTarget:self action:@selector(handlePinchGes:)];
    [self.view addGestureRecognizer:pinchGes];
    
    UITapGestureRecognizer *tapGes = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTapgesture:)];
    [self.view addGestureRecognizer:tapGes];
    
    UIPanGestureRecognizer *panGes = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(handlePangesture:)];
    [self.view addGestureRecognizer:panGes];
}

- (void)handlePinchGes:(UIPinchGestureRecognizer *)gesture {
    switch (gesture.state) {
        case UIGestureRecognizerStateBegan:
            self.originScale = self.matrix.columns[0].x;
            break;
        case UIGestureRecognizerStateChanged:
        {
            CGFloat scale = self.originScale * gesture.scale;
            [self scaleMatrix:scale y:scale z:1];
        }
        default:
            break;
    }
}

- (ColorBoxModel *)findColorBox:(CGFloat)x y:(CGFloat)y {
    for (NSString *key in self.colorDic.allKeys) {
        ColorBoxModel *model = [self.colorDic objectForKey:key];
        if (x < model.minX || y < model.minY) {
            continue;
        }
        if (x > model.maxX || y > model.maxY) {
            continue;
        }
        return model;
    }
    return nil;
}


- (void)handleTapgesture:(UITapGestureRecognizer *)gesture {
    CGPoint location = [gesture locationInView:self.view];
    CGFloat width =  self.view.bounds.size.width;
    CGFloat height = self.view.bounds.size.height;
    
    CGFloat scale = self.matrix.columns[0].x;
    CGFloat transX = self.matrix.columns[3].x;
    CGFloat transY = self.matrix.columns[3].y;
    
    CGFloat scaledWidth = scale * width;
    
    CGFloat marginTop = (height - scaledWidth) / 2.0;
    CGFloat marginLeft = (scaledWidth - width) / 2.0;
    
    location.y -= marginTop;
    location.x += marginLeft;
    
    CGFloat x = (self.pixWidth / scaledWidth) * location.x - scaledWidth * transX;
    CGFloat y = (self.pixWidth / scaledWidth) * location.y - scaledWidth * transY;
    
    ColorBoxModel *colorBox = [self findColorBox:x y:y];
    if (!colorBox) {
        return;
    }
    
    RegionModel *region = [self.regionDic objectForKey:colorBox.number];
//    for (RegionModel *model in self.regionDic.allValues) {
//        if (model.iscolored) {
//            continue;
//        }
//        region = model;
//        break;
//    }
    if (!region) {
        return;
    }
//    ColorBoxModel *colorBox = [self.colorDic objectForKey:@(region.colorNumber).stringValue];
//    if (!colorBox) {
//        return;
//    }
    unsigned r = 255.0;
    unsigned g = 255.0;
    unsigned b = 255.0;
    if (self.shaderType & ShaderTypeNormal) {
        HexColor color = [region.color hex];
        r = color.r;
        g = color.g;
        b = color.b;
    }

    
    ShaderRegion regionTemp = (ShaderRegion){
        {colorBox.minX, colorBox.minY, colorBox.maxX , colorBox.maxY},
        region.colorNumber,
        {(float)r / 255.0, (float)g / 255.0,(float)b / 255.0, 1.0},
        self.pixWidth
    };
    self.regionBuffer = [self.device newBufferWithBytes:&regionTemp length:sizeof(ShaderRegion) options:MTLResourceStorageModeShared];
    region.iscolored = YES;
}

- (void)handlePangesture:(UIPanGestureRecognizer *)panGes {
    switch (panGes.state) {
        case UIGestureRecognizerStateChanged:
        {
            CGSize size = self.view.bounds.size;
            CGPoint trans  = [panGes translationInView:self.view];
            CGFloat x = trans.x / size.width;
            CGFloat y = trans.y / size.height;
            [self moveMatrix:x y:-y z:0];
            [panGes setTranslation:CGPointZero inView:self.view];
        }
            break;
            
        default:
            break;
    }
}



#pragma mark - vertex

- (void)initShaderVertexs {
    CGSize size = [UIScreen mainScreen].bounds.size;
    CGFloat scale = size.width / size.height;
    _vertexs[0] = (ShaderVertex){ {  -1,  scale, 0, 1},  { 0.f, 0.f } };
    _vertexs[1] = (ShaderVertex){ {  -1, -scale, 0, 1},  { 0.f, 1.f } };
    _vertexs[2] = (ShaderVertex){ {   1,  scale, 0, 1},  { 1.f, 0.f } };
    _vertexs[3] = (ShaderVertex){ {   1,   scale, 0, 1},  { 1.f, 0.f } };
    _vertexs[4] = (ShaderVertex){ {   1,  -scale, 0, 1},  { 1.f, 1.f } };
    _vertexs[5] = (ShaderVertex){ {  -1,  -scale, 0, 1},  { 0.f, 1.f } };
}

#pragma mark - transform matrix

- (void)initTranformMatrix {
    _matrix =(simd_float4x4){
        {{1, 0, 0, 0},
         {0, 1, 0, 0},
         {0, 0, 1, 0},
         {0, 0, 0, 1}}
    };
}

- (void)moveMatrix:(float)x
                 y:(float)y
                 z:(float)z {
    _matrix.columns[3].x += x;
    _matrix.columns[3].y += y;
    _matrix.columns[3].z += z;
}

- (void)scaleMatrix:(float)x
                  y:(float)y
                  z:(float)z {
    _matrix.columns[0].x = MIN(3.0, MAX(0.5, x));
    _matrix.columns[1].y = MIN(3.0, MAX(0.5, y));
    _matrix.columns[2].z = MIN(3.0, MAX(0.5, z));
}

#pragma mark - pipe line state
- (void)initPipelineState {
    self.renderPipelineState = [self makePipeLineState:self.mtview.colorPixelFormat];
    self.originPipelineState = [self makePipeLineState:self.mtview.colorPixelFormat];
    
    NSError *err;
    if (self.shaderType & ShaderTypeColored || self.shaderType & ShaderTypeGray) {
        id<MTLFunction> computFunc = [self.library newFunctionWithName:@"make_color_painted"];
        self.computePipelineState = [self.device newComputePipelineStateWithFunction:computFunc error:&err];
    } else {
        id<MTLFunction> computFunc = [self.library newFunctionWithName:@"make_color_main"];
        self.computePipelineState = [self.device newComputePipelineStateWithFunction:computFunc error:&err];
    }
    if (self.shaderType & ShaderTypeGray) {
        
        id<MTLFunction> computFunc = [self.library newFunctionWithName:@"make_color_gray"];
        self.grayPipelineState = [self.device newComputePipelineStateWithFunction:computFunc error:&err];
    }
  
    
}

- (void)initTetxure {
    int32_t count = self.pixWidth * self.pixWidth ;
    
    self.originTexture = [self loadImageTexture:[self.pid stringByAppendingString:@"_pdf"] type:@"pdf"];
    if (self.shaderType & ShaderTypeGray || self.shaderType & ShaderTypeColored) {
        self.paintedTexture = [self loadImageTexture:[self.pid stringByAppendingString:@"_colored"] type:@"jpeg"];
    }
    
    MTLTextureDescriptor *des = [MTLTextureDescriptor texture2DDescriptorWithPixelFormat:MTLPixelFormatBGRA8Unorm width:self.pixWidth height:self.pixWidth mipmapped:NO];
    UInt32 *bytes = (UInt32 *)calloc(count, sizeof(UInt32));
    memset(bytes, 0, count);
    self.paintingTexture = [self.device newTextureWithDescriptor:des];
    MTLRegion region = {{ 0, 0, 0 }, {self.pixWidth, self.pixWidth, 1}}; // 纹理上传的范围
    [self.paintingTexture replaceRegion:region mipmapLevel:0 withBytes:bytes bytesPerRow:self.pixWidth * 4];
}


- (id<MTLRenderPipelineState>)makePipeLineState:(MTLPixelFormat)format {
    MTLRenderPipelineDescriptor *descriptor2 = [MTLRenderPipelineDescriptor new];
    descriptor2.colorAttachments[0].pixelFormat = format;
    descriptor2.fragmentFunction = [self.library newFunctionWithName:@"fragmentFunc"];
    descriptor2.vertexFunction = [self.library newFunctionWithName:@"vertexFunc"];
    descriptor2.colorAttachments[0].blendingEnabled = YES;
    descriptor2.colorAttachments[0].rgbBlendOperation = MTLBlendOperationAdd;
    descriptor2.colorAttachments[0].alphaBlendOperation = MTLBlendOperationSubtract;
        descriptor2.colorAttachments[0].sourceRGBBlendFactor = MTLBlendFactorSourceAlpha;
    descriptor2.colorAttachments[0].sourceAlphaBlendFactor = MTLBlendFactorSourceAlpha;
        descriptor2.colorAttachments[0].destinationRGBBlendFactor = MTLBlendFactorOneMinusSourceAlpha;
    descriptor2.colorAttachments[0].destinationAlphaBlendFactor = MTLBlendFactorOneMinusSourceAlpha;
    NSError *err;
    id<MTLRenderPipelineState> state = [self.device newRenderPipelineStateWithDescriptor:descriptor2 error:&err];
    return state;
}


- (void)drawWithBuffer:(id<MTLCommandBuffer>)buffer view:(MTKView *)view {
    if (self.grayPipelineState) {
        id<MTLComputeCommandEncoder> encoder = [buffer computeCommandEncoder];
        [encoder setComputePipelineState:self.grayPipelineState];

        [encoder setTexture:self.paintingTexture atIndex:ShaderComputeTextureIndexPainting];
        [encoder setTexture:self.paintedTexture atIndex:ShaderComputeTextureIndexPainted];
        
        MTLSize threadsPerGroup = {16, 16, 1};
        MTLSize numThreadgroups = {self.pixWidth / threadsPerGroup.width,
            self.pixWidth / threadsPerGroup.height, 1};

        [encoder dispatchThreadgroups:numThreadgroups
                                        threadsPerThreadgroup:threadsPerGroup];
        [encoder endEncoding];
        self.grayPipelineState = nil;
    }
    if (self.regionBuffer) {
        id<MTLComputeCommandEncoder> computed = [buffer computeCommandEncoder];
        [computed setComputePipelineState:self.computePipelineState];

        [computed setTexture:self.paintingTexture atIndex:ShaderComputeTextureIndexPainting];
        if (self.shaderType & ShaderTypeColored || self.shaderType & ShaderTypeGray) {
            [computed setTexture:self.paintedTexture atIndex:ShaderComputeTextureIndexPainted];
        }
        [computed setBuffer:self.regionTexture offset:0 atIndex:ShaderComputeTextureIndexRegion];
        [computed setBuffer:self.regionBuffer offset:0 atIndex:ShaderComputeBufferIndexRegion];
        MTLSize threadsPerGroup = {16, 16, 1};
        MTLSize numThreadgroups = {self.pixWidth / threadsPerGroup.width,
            self.pixWidth / threadsPerGroup.height, 1};

        [computed dispatchThreadgroups:numThreadgroups
                                        threadsPerThreadgroup:threadsPerGroup];
        [computed endEncoding];
        self.regionBuffer = nil;
    }
    MTLRenderPassDescriptor * descriptor =[view currentRenderPassDescriptor];
    if (!descriptor) {
        return;
    }
    descriptor.colorAttachments[0].loadAction = MTLLoadActionClear;
    descriptor.colorAttachments[0].storeAction = MTLStoreActionStore;
    descriptor.colorAttachments[0].clearColor = MTLClearColorMake(1.0,1.0,1.0,1.0);
//    descriptor.colorAttachments[1].texture = self.whiteTexture;
    
    id<MTLRenderCommandEncoder> encoder = [buffer renderCommandEncoderWithDescriptor:descriptor];
    [encoder setViewport:(MTLViewport){0.0, 0.0, self.viewPort.x, self.viewPort.y, 0, 2.0 }];
   
    [encoder setVertexBytes:_vertexs length:sizeof(_vertexs) atIndex:ShaderVertexInputIndexVertex];
    [encoder setVertexBytes:&_matrix length:sizeof(simd_float4x4) atIndex:ShaderVertexInputIndexMatrix];
    
    [encoder setRenderPipelineState:self.renderPipelineState];
    
    [encoder setFragmentTexture:self.paintingTexture atIndex:ShaderFragmentInputIndexVertex];
    [encoder drawPrimitives:MTLPrimitiveTypeTriangle
                vertexStart:0
                vertexCount:6];
    
    if (self.shaderType & ShaderTypeNormal || self.shaderType & ShaderTypeColored) {
        [encoder setRenderPipelineState:self.originPipelineState];
        [encoder setFragmentTexture:self.originTexture atIndex:ShaderFragmentInputIndexVertex];
    }
    

    [encoder drawPrimitives:MTLPrimitiveTypeTriangle
                vertexStart:0
                vertexCount:6];
    
    [encoder endEncoding];
    
    
}

@end
