//
//  BaseViewController.m
//  Shader
//
//  Created by mac on 2021/4/24.
//  Copyright © 2021 com.zhenghong. All rights reserved.
//

#import "BaseViewController.h"
#import "UIImage+Bundle.h"
@interface BaseViewController ()

@end

@implementation BaseViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self commonInit];
}

- (void)commonInit {
    self.device = MTLCreateSystemDefaultDevice();
    self.library = [self.device newDefaultLibrary];
    self.mtview = [[MTKView alloc] initWithFrame:self.view.bounds device:self.device];
    self.mtview.delegate = self;
    [self.view insertSubview:self.mtview atIndex:0];
    [self mtkView:self.mtview drawableSizeWillChange:self.mtview.drawableSize];
    self.commandQueue = [self.device newCommandQueue];
    
}
- (void)drawWithBuffer:(id<MTLCommandBuffer>)buffer view:(MTKView *)view {
    
}

- (id<MTLTexture>)loadImageTexture:(NSString *)imageName type:(NSString *)type {
    UIImage *image = [UIImage bundle:imageName type:type];
    MTLTextureDescriptor *textureDes = [MTLTextureDescriptor new];
    textureDes.height = image.size.height;
    textureDes.width = image.size.width;
    textureDes.pixelFormat = MTLPixelFormatRGBA8Unorm;
    
    
    id<MTLTexture> texture = [self.device newTextureWithDescriptor:textureDes];
    
    Byte *bytes = [image bytes];
    MTLRegion region = {{ 0, 0, 0 }, {image.size.width, image.size.height, 1}}; // 纹理上传的范围
    if (bytes) {
        
        [texture replaceRegion:region mipmapLevel:0 withBytes:bytes bytesPerRow:4 * image.size.width];
        free(bytes);
        bytes = nil;
        return texture;
    }
    return nil;
}


#pragma mark - MTKViewDelegate
- (void)mtkView:(MTKView *)view drawableSizeWillChange:(CGSize)size {
    _viewPort.x = size.width;
    _viewPort.y = size.height;
}

- (void)drawInMTKView:(MTKView *)view {
    id<MTLCommandBuffer> buffer = [self.commandQueue commandBuffer];
    [self drawWithBuffer:buffer view:view];
    [buffer presentDrawable:view.currentDrawable];
    [buffer commit];
}

@end
