//
//  RegionLoader.h
//  Shader
//
//  Created by mac on 2021/4/24.
//  Copyright © 2021 com.zhenghong. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "RegionModel.h"
NS_ASSUME_NONNULL_BEGIN

@interface RegionLoader : NSObject

+ (NSDictionary<NSString *, RegionModel *> *)load:(NSString *)pid;

@end

NS_ASSUME_NONNULL_END
