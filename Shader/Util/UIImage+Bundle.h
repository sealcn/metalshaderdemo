//
//  UIImage+Bundle.h
//  Shader
//
//  Created by 郑红 on 2021/4/21.
//  Copyright © 2021 com.zhenghong. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface UIImage (Bundle)

+ (UIImage *)bundle:(NSString *)name
               type:(NSString *)type;

- (Byte *)bytes;

- (UInt32 *)bytes_32;

@end

NS_ASSUME_NONNULL_END
