//
//  UIImage+Bundle.m
//  Shader
//
//  Created by 郑红 on 2021/4/21.
//  Copyright © 2021 com.zhenghong. All rights reserved.
//

#import "UIImage+Bundle.h"

@implementation UIImage (Bundle)

+ (UIImage *)bundle:(NSString *)name
               type:(NSString *)type {
    NSString *url = [[NSBundle mainBundle] pathForResource:name ofType:type];
    if ([type isEqualToString:@"pdf"]) {
        return [self fromPdf:url]   ;
    }
    UIImage *image = [UIImage imageWithContentsOfFile:url];
    return image;
}

+ (UIImage *)fromPdf:(NSString *)pdfPath {
    NSData *pdfData = [NSData dataWithContentsOfFile:pdfPath];
    if (!pdfData) {
        return nil;
    }
    return [UIImage imageWithPDFData:pdfData scaledSize:CGSizeMake(1024, 1024) scale:1 atPageIndex:1];
}


+ (UIImage *)imageWithPDFData:(nonnull NSData *)pdfData scaledSize:(CGSize)scaledSize scale:(CGFloat)scale atPageIndex:(NSInteger)pageIndex {
    if (pdfData.length <= 0) {
        return nil;
    }
    
    if (scaledSize.width <= 0.0 || scaledSize.height <= 0.0) {
        return [UIImage new];
    }
    
    UIImage *image = nil;
    if ([NSProcessInfo processInfo].physicalMemory <= 1024458752) {
        scale = 1.0;
    }
    
    CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceRGB();
    CGContextRef context = CGBitmapContextCreateWithData(NULL, ((size_t)ceil(scaledSize.width * scale)), ((size_t)ceil(scaledSize.height * scale)), 8, 0, colorSpace, kCGBitmapByteOrderDefault | kCGImageAlphaPremultipliedFirst, nil, NULL);
    if (!context) {
        CGColorSpaceRelease(colorSpace);
        return nil;
    }
    
    CGContextScaleCTM(context, scale, scale);
    CFDataRef cfData = CFDataCreate(kCFAllocatorDefault, pdfData.bytes, pdfData.length);
    if (!cfData) {
        CGColorSpaceRelease(colorSpace);
        CGContextRelease(context);
        return nil;
    }
    CGDataProviderRef provider = CGDataProviderCreateWithCFData(cfData);
    if (!provider) {
        CGColorSpaceRelease(colorSpace);
        CGContextRelease(context);
        CFRelease(cfData);
        return nil;
    }
    CGPDFDocumentRef pdfDocument = CGPDFDocumentCreateWithProvider(provider);
    if (!pdfDocument) {
        CGColorSpaceRelease(colorSpace);
        CGContextRelease(context);
        CFRelease(cfData);
        CGDataProviderRelease(provider);
        return nil;
    }
    CGPDFPageRef pdfPage = CGPDFDocumentGetPage(pdfDocument, pageIndex);
    if (!pdfPage) {
        CGColorSpaceRelease(colorSpace);
        CGContextRelease(context);
        CFRelease(cfData);
        CGDataProviderRelease(provider);
        CGPDFDocumentRelease(pdfDocument);
        return nil;
    }
    
    CGRect desRect = CGRectMake(0.0, 0, scaledSize.width, scaledSize.height);
    CGAffineTransform transform = CGPDFPageGetDrawingTransform(pdfPage, kCGPDFCropBox, desRect, 0, true);
    CGContextConcatCTM(context, transform);
    CGContextDrawPDFPage(context, pdfPage);
    CGImageRef cgImage = CGBitmapContextCreateImage(context);
    if(cgImage) {
        image = [UIImage imageWithCGImage:cgImage scale:scale orientation:UIImageOrientationUp];
    }
    
    CGColorSpaceRelease(colorSpace);
    CGContextRelease(context);
    CFRelease(cfData);
    CGDataProviderRelease(provider);
    CGPDFDocumentRelease(pdfDocument);
    CGImageRelease(cgImage);
    
    return image;
}


- (Byte *)bytes {
    CGImageRef cgimage = self.CGImage;
    size_t width = CGImageGetWidth(cgimage);
    size_t height = CGImageGetHeight(cgimage);
    Byte *data = (Byte *)calloc(width * height * 4, sizeof(Byte));
    
    CGContextRef context = CGBitmapContextCreate(data, width, height, 8, width * 4, CGImageGetColorSpace(cgimage), kCGImageAlphaPremultipliedLast);
    CGContextDrawImage(context, CGRectMake(0, 0, width, height), cgimage);
    CGContextRelease(context);
    return data;
}
- (UInt32 *)bytes_32 {
    CGImageRef cgimage = self.CGImage;
    size_t width = CGImageGetWidth(cgimage);
    size_t height = CGImageGetHeight(cgimage);
    UInt32 *data = (UInt32 *)calloc(width * height , sizeof(UInt32));
    
    CGContextRef context = CGBitmapContextCreate(data, width, height, 8, width * 4, CGImageGetColorSpace(cgimage), kCGImageAlphaPremultipliedLast);
    CGContextDrawImage(context, CGRectMake(0, 0, width, height), cgimage);
    CGContextRelease(context);
    return data;
}

@end
