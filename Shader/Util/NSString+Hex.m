//
//  NSString+Hex.m
//  Shader
//
//  Created by mac on 2021/4/26.
//  Copyright © 2021 com.zhenghong. All rights reserved.
//

#import "NSString+Hex.h"

@implementation NSString (Hex)
- (HexColor)hex {
    if (self.length == 0) {
        return (HexColor){
            1,
            1,
            1};
    }
    NSString *string = self;
    if([self characterAtIndex:0] != '#') {
        string = [NSString stringWithFormat:@"#%@", string];
    }
    
    NSString *redHex = [NSString stringWithFormat:@"0x%@", [string substringWithRange:NSMakeRange(1, 2)]];
    unsigned red = [string hexValueToUnsigned:redHex];
    
    NSString *greenHex = [NSString stringWithFormat:@"0x%@", [string substringWithRange:NSMakeRange(3, 2)]];
    unsigned green = [string hexValueToUnsigned:greenHex];
    
    NSString *blueHex = [NSString stringWithFormat:@"0x%@", [string substringWithRange:NSMakeRange(5, 2)]];
    unsigned blue = [string hexValueToUnsigned:blueHex];
    
    return (HexColor) {
        red,
        green,
        blue
    };
}

- (unsigned)hexValueToUnsigned:(NSString *)hexValue {
    unsigned value = 0;
    NSScanner *hexValueScanner = [NSScanner scannerWithString:hexValue];
    [hexValueScanner scanHexInt:&value];
    return value;
}


@end
