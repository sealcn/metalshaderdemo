//
//  RegionLoader.m
//  Shader
//
//  Created by mac on 2021/4/24.
//  Copyright © 2021 com.zhenghong. All rights reserved.
//

#import "RegionLoader.h"


@implementation RegionLoader
+ (NSDictionary<NSString *,RegionModel *> *)load:(NSString *)pid {
    NSString *name = [NSString stringWithFormat:@"%@_detail", pid];
    NSString *path = [[NSBundle mainBundle] pathForResource:name ofType:@"json"];
    NSData *data = [NSData dataWithContentsOfFile:path];
    NSError *err;
    NSDictionary *jsonDic = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:&err];
    NSString *plansStr = [jsonDic objectForKey:@"plans"][0];
    NSArray *plans = [plansStr componentsSeparatedByString:@"|"];
    NSMutableDictionary *colors = @{}.mutableCopy;
    for (NSString *plan in plans) {
        NSArray *colorandnums = [plan componentsSeparatedByCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@",#"]];
        NSString *color = [colorandnums lastObject];
        for (NSString *s in colorandnums) {
            if ([s containsString:@"#"]) {
                continue;
            }
            [colors setValue:color forKey:s];
        }
    }
    
    
    NSString *center = [jsonDic objectForKey:@"center"];
    NSArray *entrys = [center componentsSeparatedByString:@"|"];
    NSMutableDictionary *r = @{}.mutableCopy;
    for (NSString * entry in entrys) {
        NSArray *infos =  [entry componentsSeparatedByString:@","] ;
        RegionModel *region = [RegionModel new];
        NSString *number = [infos objectAtIndex:0];
        NSString *xs = [infos objectAtIndex:1];
        NSString *ys = [infos objectAtIndex:2];
        NSString *sizes = [infos objectAtIndex:3];
        
        region.colorNumber = [number intValue];
        region.x = [xs intValue];
        region.y = [ys intValue];
        region.size = [sizes intValue];
        region.color = [colors objectForKey:number];
        
        [r setValue:region forKey:number];
    }
    return r.mutableCopy;
}
@end
