//
//  ColorBoxLoader.m
//  Shader
//
//  Created by mac on 2021/4/24.
//  Copyright © 2021 com.zhenghong. All rights reserved.
//

#import "ColorBoxLoader.h"

@implementation ColorBoxLoader


+ (NSDictionary<NSString *,ColorBoxModel *> *)load:(NSString *)pid {
    NSString *name = [NSString stringWithFormat:@"%@_colorBox", pid];
    NSString *path = [[NSBundle mainBundle] pathForResource:name ofType:@"json"];
    NSData *data = [NSData dataWithContentsOfFile:path];
    NSError *err;
    NSDictionary *jsonDic = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:&err];
  
    NSMutableDictionary *r = @{}.mutableCopy;
    for (NSString *key in jsonDic.allKeys) {
        NSDictionary *obj = [jsonDic objectForKey:key];
        ColorBoxModel *boxModel = [ColorBoxModel new];
        boxModel.maxX = [[obj objectForKey:@"maxX"] floatValue];
        boxModel.maxY = [[obj objectForKey:@"maxY"] floatValue];
        boxModel.minX = [[obj objectForKey:@"minX"] floatValue];
        boxModel.minY = [[obj objectForKey:@"minY"] floatValue];
        boxModel.number = key;
        [r setValue:boxModel forKey:key];
    }
    
    return r.copy;
}

@end
