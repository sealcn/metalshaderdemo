//
//  NSString+Hex.h
//  Shader
//
//  Created by mac on 2021/4/26.
//  Copyright © 2021 com.zhenghong. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

typedef struct {
    unsigned r;
    unsigned g;
    unsigned b;
} HexColor;

@interface NSString (Hex)

- (HexColor)hex;

@end

NS_ASSUME_NONNULL_END
